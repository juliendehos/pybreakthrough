from setuptools import setup

setup(name='pybreakthrough',
      version='0.1.0',
      packages=[''],
      package_dir={'': 'src'},
      scripts=['scripts/breakthrough-bots.py',
               'scripts/breakthrough-cli.py',
               'scripts/breakthrough-test.py',
               'scripts/breakthrough-unittest.py'],
      install_requires=['numpy'])
