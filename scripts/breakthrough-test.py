#! /usr/bin/env python3
from copy import deepcopy, copy
import Breakthrough as bt

g1 = bt.Game(8, 8)
g2 = copy(g1)
g3 = deepcopy(g1)
g1.play((1, 0, 2, 0))

print(g1.board, "\n")
print(g2.board, "\n")
print(g3.board, "\n")
