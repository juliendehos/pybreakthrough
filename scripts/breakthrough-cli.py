#! /usr/bin/env python3
import Breakthrough as bt


def printGame(g):
    print(g.board)
    print("player", g.current)
    print("winner", g.winner)
    print("moves", g.moves)


g = bt.Game(8, 8)
printGame(g)

while True:
    i0 = int(input("i0 ? "))
    j0 = int(input("j0 ? "))
    i1 = int(input("i1 ? "))
    j1 = int(input("j1 ? "))
    m = (i0, j0, i1, j1)
    g.play(m)
    printGame(g)
    if g.winner != bt.NONE:
        break
