#! /usr/bin/env python3
import Breakthrough as bt
import BotRandom
import BotMc

nbRuns = 10
nbWins = [0, 0]
bots = [BotRandom.BotRandom(), BotMc.BotMc(100)]
# bots = [BotRandom.BotRandom(), BotRandom.BotRandom()]
# bots = [BotMc.BotMc(100), BotMc.BotMc(100)]

iWhite, iBlack = 0, 1
for n in range(nbRuns):
    g = bt.Game(8, 8)
    while g.winner == bt.NONE:
        i = iWhite if g.current == bt.WHITE else iBlack
        m = bots[i].genmove(g)
        g.play(m)
    if g.winner == bt.WHITE:
        nbWins[iWhite] += 1
    else:
        nbWins[iBlack] += 1
    iWhite, iBlack = iBlack, iWhite

print(nbWins)
