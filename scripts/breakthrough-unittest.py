#! /usr/bin/env python3
import unittest
import Breakthrough as bt


class TestBreakthrough(unittest.TestCase):

    def test_init(self):
        g = bt.Game(8, 8)
        self.assertEqual(g.nbI, 8)
        self.assertEqual(g.nbJ, 8)
        self.assertEqual(g.current, bt.WHITE)
        self.assertEqual(g.next, bt.BLACK)
        self.assertEqual(g.winner, bt.NONE)

    def test_init_board(self):
        g = bt.Game(6, 8)
        for j in range(8):
            self.assertEqual(g.board[0, j], bt.WHITE)
            self.assertEqual(g.board[1, j], bt.WHITE)
            self.assertEqual(g.board[2, j], bt.NONE)
            self.assertEqual(g.board[3, j], bt.NONE)
            self.assertEqual(g.board[4, j], bt.BLACK)
            self.assertEqual(g.board[5, j], bt.BLACK)

    # TODO


if __name__ == '__main__':
    unittest.main()
