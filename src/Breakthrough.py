#! /usr/bin/env python3
import numpy as np

NONE = np.int(0)
WHITE = np.int(1)
BLACK = np.int(2)


class Game:
    def __init__(self, nbI, nbJ):
        self.nbI = nbI
        self.nbJ = nbJ
        self.current = WHITE
        self.next = BLACK
        self.winner = NONE

        self.board = np.ndarray((nbI, nbJ), dtype=np.int)
        self.board[-2:, :] = BLACK
        self.board[2:-2, :] = NONE
        self.board[0:2, :] = WHITE

        self.moves = _moves(self.board, self.nbI, self.nbJ, self.current)

    def play(self, m):
        if (m not in self.moves) or (self.winner != NONE):
            return False
        else:
            (i0, j0, i1, j1) = m
            self.board[i0, j0] = NONE
            self.board[i1, j1] = self.current
            self.current, self.next = self.next, self.current
            if np.where(self.board[0:1, :] == BLACK)[0].size != 0:
                self.winner = BLACK
            elif np.where(self.board[-1:, :] == WHITE)[0].size != 0:
                self.winner = WHITE
            self.moves = _moves(self.board, self.nbI, self.nbJ, self.current)
            return True


def _moves(board, nbI, nbJ, player):
    iMin, iMax, iInc = (1, nbI, -1) if player == BLACK else (0, nbI-1, 1)
    return [(i0, j0, i0+iInc, j1)
            for i0 in range(iMin, iMax)
            for j0 in range(nbJ)
            for j1 in [j0-1, j0, j0 + 1]
            if j1 >= 0
            and j1 < nbJ
            and board[i0, j0] == player
            and (board[i0+iInc, j1] == NONE
                 or j0 != j1 and board[i0, j0] != board[i0+iInc, j1])]
