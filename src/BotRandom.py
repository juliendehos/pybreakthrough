#! /usr/bin/env python3
import random


class BotRandom:
    def __init__(self):
        self.rng = random.Random()

    def genmove(self, game):
        moves = game.moves
        return self.rng.choice(moves)
