#! /usr/bin/env python3
import random
import Breakthrough as bt
from copy import deepcopy


class BotMc:
    def __init__(self, nbPlayouts):
        self.rng = random.Random()
        self.nbPlayouts = nbPlayouts

    def genmove(self, game):
        moves = game.moves
        nbMoves = len(moves)
        nbPlayoutsMove = max(1, self.nbPlayouts // nbMoves)
        scores = [(self.evalmove(game, m, nbPlayoutsMove), m) for m in moves]
        _, bestMove = max(scores)
        return bestMove

    def evalmove(self, game, move, nbPlayoutsMove):
        player = game.current
        game2 = deepcopy(game)
        game2.play(move)
        score = 0
        for i in range(nbPlayoutsMove):
            score += 1 if self.playout(game2) == player else 0
        return score

    def playout(self, game):
        g = deepcopy(game)
        while g.winner == bt.NONE:
            m = self.rng.choice(g.moves)
            g.play(m)
        return g.winner
