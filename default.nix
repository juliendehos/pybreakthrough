{ pkgs ? import <nixpkgs> {} }:

with pkgs.python3Packages;

buildPythonPackage {
  name = "pybreakthrough";
  src = ./.;
  buildInputs = [
    numpy
  ];
}

